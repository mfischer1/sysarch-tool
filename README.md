# System Architecture Tool

The Sytem Architecture Tool is an application for the graphical and dynamic representation and linking of overview graphics, documentation and other project information with arbitrarily deep levels of detail.

[gitlab](https://gitlab.com/mfischer1/sysarch-tool.git)


## Installing / Starting the Application Locally

### Prerequisite: A Relational Database

MariaDB is currently used as database. It can be set up manually or by using the following container:

[https://github.com/docker-library/mariadb/blob/master/10.4/Dockerfile](https://github.com/docker-library/mariadb/blob/master/10.4/Dockerfile)

```
docker run -p 3306:3306 --name sysarch_db -e MYSQL_ROOT_PASSWORD=test -d mariadb/server
```

Alternatively you can start the database with the attached docker-compose-file (see docker-compose.yml):

```
docker-compose up
```

Instead of MariaDB any other relational database can be used.
This must then be configured accordingly in *backend/src/main/resources/application.properties* and a respective driver needs to be added to the maven dependencies.


### Building and Starting the Application

**Requirements:**
- Java 8 or greater
- Maven
- Nodejs

**Building and Starting the Application:**
1. Configuration of the database in *backend/src/main/resources/application.properties*. 
2. Execute 'mvn clean install' in the main project. This first builds and copies the frontend into the backend project under *backend/src/main/resources/public*. Then the backend is built. 
3. If necessary, create users in the database, see Readme of the backend project. 
   Alternatively, the VM argument *-Dspring.profiles.active=inittestuser* can be passed when starting the application in the next step.
   As a result, the configuration class de.hfu.doubleslash.architecturetool.config.UserInit is used and a hard-coded test user is created.
4. Make sure you have the Lombok-Plugin installed in your IDE, otherwise you'll get a Compile error.   
5. The application can now be started using the main class *de.hfu.doubleslash.architecturetool.ArchitectureToolApplication* in the backend project.

Now the application is available at [http://localhost:8080](http://localhost:8080).

## Deploying the Application to Heroku Cloud 

The application can be deployed in the Heroku Cloud for public usage. This can be achieved with the following steps:

1. Create Heroku Account
2. Create new App in Heroku
3. Add a database (e.g. ClearDB MySQL) in the addon section of Heroku(you will need a credit card)
4. In order to use the database with your app correctly, you have to add these Config Vars as keys in the 'Settings' section.
    * SPRING_DATASOURCE_URL
    * SPRING_DATASOURCE_USERNAME
    * SPRING_DATASOURCE_PASSWORD
5. In order to open your app, you have to add your App-URL in the server.js-File in Frontend to the whitelist-Array
6. Add remote to your local repository with your Heroku app name:
``
heroku git:remote -a "your-app-name"
``
7. You can use the git remote command to confirm that a remote named heroku has been set for your app: 
``
git remote -v
``
8. Push the repository to Heroku: 
``
git push heroku master
``
9. If you want to push a specific branch to Heroku:
``
git push heroku "your-branch":master
``
10. In order to open the App, deployed to Heroku, type in Command Line: 
``
heroku open
``

For more information, visit: https://devcenter.heroku.com/articles/git

### Enabling Social Registration / Login

#### Configuration for Enabling Social Registration / Login

1. At first, you'll need a developer account for the respective social network.
2. Now you're able to create a new App, with OAuth Login activated.
3. For configurations, you have to set the URL-Redirection-Link in your App and fill out general information like App-Name etc.
4. The Application, generates an App-ID and App-Secret.
5. These two information have to be set in the application.properties
``
spring.security.oauth2.client.registration.google.client-id="Your Client ID"
spring.security.oauth2.client.registration.google.client-secret="Your Client Secret"
``
6. If you have step 5. completed, you must activate the Oauth-Login for Spring in SecurityController
``
.and().oauth2Login().loginPage("/login")
``

## Registration Workflow
1. If you access Register-Page, the RegistrationController executes the method which ist annotated with @GetMapping e.g registerForm()
2. The Method creates the View of the register page and will show in your browser.
3. When you've filled out the information and click on register, the data will be sent to the @PostMapping Method of RegistrationController
4. The Method gets an Object of type RegistrationForm which contains your registration information.
5. This information will be stored as a new user in the database with encrypted password.

## Login Workflow
1. Most of the work, Spring Security is doing for you. If you want to customize the login process you'll need your own @PostMapping-Method in LoginController.
2. If you click on Login with your user data, Spring Security is checking the data.
3. If your data were correct, you'll get access to the app, otherwise you'll get an error with 'wrong credentials'.

## Project History
* 2018-03: Idea for the project at [doubleSlash Net-Business GmbH](https://www.doubleslash.de/).
* 2019-04 - 2019-07: First prototype as student project at [Furtwangen University, DE](https://www.hs-furtwangen.de/), summer term 2019.
* 2020-04 - 2019-07: Further development as student project at [Furtwangen University, DE](https://www.hs-furtwangen.de/), summer term 2020.

Please find further information in the [CHANGELOG file](CHANGELOG).


## License

See [LICENSE file](LICENSE).

