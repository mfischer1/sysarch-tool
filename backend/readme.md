# backend 

## Überblick 

Dieses Modul übernimmt das starten des Webservers und die Kommunikation zwischen frontend und Datenbank. 

## Datenbank anlegen
Die Datenbank muss manuell angelegt werden. Dies kann mittels verschiedener grafischer Tools geschehen (MySQL Workbench, Microsoft Access, phpMyAdmin, Adminer, ...) oder auf verschiedene Weisen manuell. 
Die MySQL-Syntax sieht dafür wie folgt aus: 

`CREATE DATABASE IF NOT EXISTS system_architecture_tool DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci`

wobei `system_architecture_tool` für den Namen der Datenbank steht. In der `application.properties` muss die neue Datenbank entsprechend angegeben werden.
Die Tabellen werden automatisch erzeugt.

## User erstellen für den Login
| id         | name   | password                           |
|------------|--------|------------------------------------|
|   10000    | enes   | {bcrypt}$2a$10$xJ6oOCE62BjZNbJmC9lpD.1et3ht6YhlR/QcO00pZTyMBWS5JrbSi |

**ID > 10000**: Damit bei der späteren Generierung von IDs keine Fehler auftreten.

**Passwort**: Das Passwort [hier](https://www.browserling.com/tools/bcrypt) mit Rounds=10 verschlüsseln (Syntax oben gegeben). 
