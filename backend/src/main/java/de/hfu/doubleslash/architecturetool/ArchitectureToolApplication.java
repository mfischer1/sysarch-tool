package de.hfu.doubleslash.architecturetool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@ServletComponentScan
@SpringBootApplication(scanBasePackages = "de.hfu.doubleslash.architecturetool")
@EnableJpaRepositories("de.hfu.doubleslash.architecturetool.repository")
@EntityScan("de.hfu.doubleslash.architecturetool.domain")
@ComponentScan({"de.hfu.doubleslash.architecturetool.service", "de.hfu.doubleslash.architecturetool.controller", "de.hfu.doubleslash.architecturetool.config"})
public class ArchitectureToolApplication{
    public static void main(String[] args) {
        SpringApplication.run(ArchitectureToolApplication.class, args);
    }
}
