package de.hfu.doubleslash.architecturetool.config;

import lombok.Data;
import org.springframework.security.crypto.password.PasswordEncoder;
import de.hfu.doubleslash.architecturetool.domain.UserData;

@Data
public class RegistrationForm {

    private String name;
    private String password;

    public UserData toUser(PasswordEncoder passwordEncoder){
        return new UserData(name, passwordEncoder.encode(password));
    }

}


