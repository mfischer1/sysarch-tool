package de.hfu.doubleslash.architecturetool.config;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;


@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/register").permitAll()
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .antMatchers("/login").permitAll()
                .and().oauth2Login().loginPage("/login")
                .and().csrf().disable();
        http.cors().disable();
        super.configure(http);
       http.formLogin().loginPage("/login");
    }

}
