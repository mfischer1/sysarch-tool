/*
 * Copyright (c) 2017 by BMW AG, all rights reserved.
 */
package de.hfu.doubleslash.architecturetool.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import de.hfu.doubleslash.architecturetool.ArchitectureToolApplication;
import de.hfu.doubleslash.architecturetool.domain.UserData;
import de.hfu.doubleslash.architecturetool.repository.UserDataRepository;

@Component
@Profile("inittestuser")
public class UserInit {

	private final Logger logger = LoggerFactory.getLogger(ArchitectureToolApplication.class);

	@Autowired
	private UserDataRepository userRepo;

	@EventListener(ApplicationReadyEvent.class)
	public void doSomethingAfterStartup() {

		logger.info("Init DB users.");

		// Example user with clear text password 
		// (noop means no encyption algorith. others such as bcrypt are possible.):
		createUser("user", "{noop}test");

	}

	private void createUser(String username, String password) {
		if (userRepo.findByName(username) == null) {
			UserData user = new UserData(username);
			user.setPassword(password);
			userRepo.save(user);
			logger.info("Created user '{}'.", username);
		} else {
			logger.info("User '{}' already exists.", username);
		}
	}


}
