package de.hfu.doubleslash.architecturetool.controller;

import com.google.gson.Gson;
import de.hfu.doubleslash.architecturetool.domain.*;
import de.hfu.doubleslash.architecturetool.repository.FileManagerRepository;
import de.hfu.doubleslash.architecturetool.repository.ProjectDataRepository;
import de.hfu.doubleslash.architecturetool.repository.UserDataRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController()
@RequestMapping("/api")
@EnableAutoConfiguration
public class ApiController {
    private final Logger logger = LoggerFactory.getLogger(ApiController.class);

    private final Gson gson = new Gson();

    @Autowired
    private ProjectDataRepository projectDataRepository;
    @Autowired
    private UserDataRepository userDataRepository;
    @Autowired
    private FileManagerRepository fileManagerRepository;

    @GetMapping("/createProject/{username}/{projectTitle}/{projectDescription}/{workbenchWidth}/{workbenchHeight}")
    public String createProject(
            @PathVariable String projectTitle,
            @PathVariable String projectDescription,
            @PathVariable String username,
            @PathVariable int workbenchWidth,
            @PathVariable int workbenchHeight
    ) {
        UserData oUser = userDataRepository.findByName(username);
        ProjectData projectData;

        // User will be stored in DB if using OAuth (should be optimized)
        if(oUser==null){
            UserData user = new UserData();
            user.setPassword("");
            user.setName(username);
            userDataRepository.save(user);
            oUser=user;
        }

        if (oUser != null) {
            projectData = new ProjectData(projectTitle, oUser, projectDescription, workbenchWidth, workbenchHeight);
            fileManagerRepository.save(projectData.getFileManager());
            projectDataRepository.save(projectData);
            logger.info("Project " + projectData.getProjectName() + " saved!");
            return gson.toJson(projectData);
        }
        logger.info("User not registered.");
        return gson.toJson("0");
    }

    @GetMapping("/getProject/{projectId}")
    public String getProject(
            @PathVariable long projectId
    ) {
        Optional<ProjectData> project = projectDataRepository.findById(projectId);
        if (project.isPresent()) {
            return gson.toJson(project.get());
        }
        return gson.toJson("Project not available.");
    }

    @GetMapping("/editProjectSettings/{projectId}/{projectTitle}/{projectDescription}/{workbenchWidth}/{workbenchHeight}")
    public String editProjectSettings(
            @PathVariable long projectId,
            @PathVariable String projectTitle,
            @PathVariable String projectDescription,
            @PathVariable int workbenchWidth,
            @PathVariable int workbenchHeight
    ) {
        Optional<ProjectData> project = projectDataRepository.findById(projectId);

        if (project.isPresent()) {
            project.get().setProjectName(projectTitle);
            project.get().setProjectDescription(projectDescription);
            project.get().setWorkbenchWidth(workbenchWidth);
            project.get().setWorkbenchHeight(workbenchHeight);
            projectDataRepository.flush();
            return gson.toJson("1");
        }
        return gson.toJson("0");
    }

    @GetMapping("/deleteProject/{projectId}")
    public String deleteProject(
            @PathVariable long projectId
    ) {
        Optional<ProjectData> project = projectDataRepository.findById(projectId);
        project.ifPresent(value -> projectDataRepository.delete(value));
        return gson.toJson("1");
    }

    @GetMapping("/deleteProjectFile/{projectId}/{fileId}")
    public String deleteProjectFile(
            @PathVariable long projectId,
            @PathVariable long fileId
    ) {
        Optional<ProjectData> project = projectDataRepository.findById(projectId);
        project.ifPresent(value -> value.getFileManager().removeFileById(fileId));
        return gson.toJson("1");
    }

    @GetMapping("/editProjectFile/{projectId}/{fileId}/{fileTitle}")
    public String editProjectFile(
            @PathVariable long projectId,
            @PathVariable long fileId,
            @PathVariable String fileTitle
    ) {
        Optional<ProjectData> project = projectDataRepository.findById(projectId);
        if (project.isPresent()) {
            Optional<File> file = project.get().getFileManager().renameFile(fileId, fileTitle);
            if (file.isPresent()) {
                return gson.toJson(file.get());
            }
        }
        return gson.toJson("0");
    }

    @GetMapping("/send/{content}")
    public String send(
            @PathVariable String content
    ) {
        return new String(Base64.getDecoder().decode(content));
    }

    @GetMapping(value = "/username")
    public String getAllUserData(HttpServletRequest request) {
        String name = request.getUserPrincipal().getName();
        List<String> collect = projectDataRepository.findAll()
                .stream()
                .filter(p -> p.getUser().getName().equals(name)).map(projectData -> ("{\"id\": \"" + projectData.getId() + "\", \"title\" : \"" + projectData.getProjectName() + "\"}"))
                .collect(Collectors.toList());
        String projectList = "[";
        if (!collect.isEmpty()) {
            for (int index = 0; index < collect.size(); index++) {
                projectList += collect.get(index);
                if (index != collect.size() - 1) {
                    projectList += ",";
                }
            }
        }
        projectList += "]";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username", name);
        jsonObject.put("projects", "projectList");
        String json = jsonObject.toString();
        json = json.replace("\"projectList\"", projectList);
        return json;
    }

    @GetMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (auth != null) {
                new SecurityContextLogoutHandler().logout(request, response, auth);
            }
        } catch (Exception e) {
            return gson.toJson(e.getMessage());
        }

        //return "redirect:/login";
        return gson.toJson("1");
    }

    @PostMapping("/saveProjectLayers")
    public String saveProject(
            @RequestBody String content
    ) {
        JSONObject jsonObject = new JSONObject(new String(Base64.getDecoder().decode(content)));

        Optional<ProjectData> optionalProject = projectDataRepository.findById(jsonObject.getLong("projectId"));

        List<Long> requestLayerIds = new ArrayList<>();

        if (optionalProject.isPresent()) {
            ProjectData project = optionalProject.get();
            JSONArray jsonArrayLayers = jsonObject.getJSONArray("projectLayers");

            for (int i = 0; i < jsonArrayLayers.length(); i++) {
                JSONObject jsonObjectLayer = jsonArrayLayers.getJSONObject(i);
                Layer requestLayer = gson.fromJson(jsonObjectLayer.toString(), Layer.class);
                requestLayerIds.add(requestLayer.getObjectId());
                boolean createNewLayer = true;

                for (Layer projectLayer : project.getLayers()) {
                    if (projectLayer.getObjectId() == requestLayer.getObjectId()) {
                        projectLayer.setTitle(requestLayer.getTitle());
                        projectLayer.setParentLayerId(requestLayer.getParentLayerId());
                        projectLayer.setTreeDepth(requestLayer.getTreeDepth());
                        projectLayer.setPositionX(requestLayer.getPositionX());
                        projectLayer.setPositionY(requestLayer.getPositionY());
                        projectLayer.setWidth(requestLayer.getWidth());
                        projectLayer.setHeight(requestLayer.getHeight());
                        projectLayer.setBackgroundImageId(requestLayer.getBackgroundImageId());

                        List<Long> requestDragItemIds = new ArrayList<>();

                        for (DragItem requestDragItem : requestLayer.getDragItems()) {
                            requestDragItemIds.add(requestDragItem.getObjectId());
                            boolean createNewDragItem = true;

                            for (DragItem projectDragItem : projectLayer.getDragItems()) {
                                if (projectDragItem.getObjectId() == requestDragItem.getObjectId()) {
                                    projectDragItem.setPositionX(requestDragItem.getPositionX());
                                    projectDragItem.setPositionY(requestDragItem.getPositionY());
                                    projectDragItem.setHeight(requestDragItem.getHeight());
                                    projectDragItem.setWidth(requestDragItem.getWidth());
                                    projectDragItem.setType(requestDragItem.getType());
                                    projectDragItem.setSpecificValue(requestDragItem.getSpecificValue());

                                    createNewDragItem = false;
                                    break;
                                }
                            }

                            if (createNewDragItem) {
                                projectLayer.addDragItem(new DragItem(
                                        requestDragItem.getObjectId(),
                                        requestDragItem.getPositionX(),
                                        requestDragItem.getPositionY(),
                                        requestDragItem.getHeight(),
                                        requestDragItem.getWidth(),
                                        requestDragItem.getType(),
                                        requestDragItem.getSpecificValue()
                                ));
                            }
                        }

                        List<DragItem> deleteDragItems = new ArrayList<>();

                        for (DragItem projectDragItem : projectLayer.getDragItems()) {
                            if (!requestDragItemIds.contains(projectDragItem.getObjectId())) {
                                deleteDragItems.add(projectDragItem);
                            }
                        }

                        for (DragItem dragItem : deleteDragItems) {
                            projectLayer.deleteDragItem(dragItem);

                        }
                        createNewLayer = false;
                        break;
                    }
                }

                if (createNewLayer) {
                    Layer newProjectLayer = new Layer(
                            requestLayer.getTitle(),
                            requestLayer.getParentLayerId(),
                            requestLayer.getTreeDepth(),
                            requestLayer.getPositionX(),
                            requestLayer.getPositionY(),
                            requestLayer.getWidth(),
                            requestLayer.getHeight(),
                            requestLayer.getBackgroundImageId(),
                            requestLayer.getObjectId());

                    for (DragItem requestDragItem : requestLayer.getDragItems()) {
                        newProjectLayer.addDragItem(new DragItem(
                                requestDragItem.getObjectId(),
                                requestDragItem.getPositionX(),
                                requestDragItem.getPositionY(),
                                requestDragItem.getHeight(),
                                requestDragItem.getWidth(),
                                requestDragItem.getType(),
                                requestDragItem.getSpecificValue()
                        ));
                    }

                    project.addLayer(newProjectLayer);
                }
            }
            List<Layer> deleteLayers = new ArrayList<>();

            for (Layer projectLayer : project.getLayers()) {
                if (!requestLayerIds.contains(projectLayer.getObjectId())) {
                    deleteLayers.add(projectLayer);
                }
            }

            for (Layer layer : deleteLayers) {
                project.deleteLayer(layer);
            }

            projectDataRepository.saveAndFlush(project);

            return gson.toJson("1");
        }

        return gson.toJson("0");
    }

}
