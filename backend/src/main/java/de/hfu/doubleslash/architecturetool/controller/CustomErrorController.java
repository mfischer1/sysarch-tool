package de.hfu.doubleslash.architecturetool.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomErrorController implements ErrorController {
    @RequestMapping("/error")
    public String handleError() {
        return "Not supported 404. (Error)";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
