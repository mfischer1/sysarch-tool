package de.hfu.doubleslash.architecturetool.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController()
@RequestMapping("/file")
@EnableAutoConfiguration
public class FileController {
    @Value("${fileUploadDefaultDirectory}")
    private String fileUploadDefaultDirectory;

    @GetMapping("/get/{filename}")
    public ResponseEntity<InputStreamResource> getFile(
            @PathVariable String filename
    ) {
        try {
            filename = filename.replace("/", "").replace("\\", "").replace("..", "");

            File requestedFile = new File(fileUploadDefaultDirectory + "\\" + filename);
            InputStream fileInputStream = new FileInputStream(requestedFile);
            String mimeType = Files.probeContentType(requestedFile.toPath());

            return ResponseEntity
                    .ok()
                    .contentType(MediaType.parseMediaType(mimeType))
                    .body(new InputStreamResource(fileInputStream));
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

}
