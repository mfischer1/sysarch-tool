package de.hfu.doubleslash.architecturetool.controller;

import com.google.gson.Gson;
import de.hfu.doubleslash.architecturetool.domain.File;
import de.hfu.doubleslash.architecturetool.domain.ProjectData;
import de.hfu.doubleslash.architecturetool.repository.FileManagerRepository;
import de.hfu.doubleslash.architecturetool.repository.ProjectDataRepository;
import de.hfu.doubleslash.architecturetool.repository.UserDataRepository;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RequestMapping("/api")
@EnableAutoConfiguration
public class FileUploadController {
    private final Logger logger = LoggerFactory.getLogger(FileUploadController.class);
    private final Gson gson = new Gson();
    @Autowired
    UserDataRepository userDataRepository;
    @Autowired
    FileManagerRepository fileManagerRepository;
    @Value("${application.name}")
    private String applicationName;
    @Value("${fileUploadDefaultDirectory}")
    private String fileUploadDefaultDirectory;
    @Autowired
    private ProjectDataRepository projectDataRepository;

    @PostMapping("/addProjectFile")
    public String addProjectFile(
            @RequestParam("projectId") long projectId,
            @RequestParam("fileTitle") String fileTitle,
            @RequestParam("fileType") String fileType,
            @RequestParam("file") MultipartFile file,
            RedirectAttributes redirectAttributes
    ) {
        File databaseFileEntry = null;

        if (!file.isEmpty()) {
            try {
                String fileId = String.valueOf(new Date().getTime());
                byte[] bytes = file.getBytes();
                String fileName = fileId + "." + FilenameUtils.getExtension(file.getOriginalFilename());
                Path path = Paths.get(fileUploadDefaultDirectory + "\\" + fileName);
                Files.write(path, bytes);
                databaseFileEntry = new File(fileName, fileTitle, fileType);

                if (projectDataRepository.findById(projectId).isPresent()) {
                    ProjectData projectData = projectDataRepository.findById(projectId).get();
                    projectData.getFileManager().addFile(databaseFileEntry);
                    projectDataRepository.save(projectData);
                }
            } catch (IOException e) {
                logger.error(e.toString());
            }
        }

        String content = "0";

        if (databaseFileEntry != null) {
            content = gson.toJson(databaseFileEntry);
        }

        content = new String(Base64.getEncoder().encode(content.getBytes())).replace("\n", "").replace("\r", "");

        return "redirect:/api/send/" + content;
    }
}


