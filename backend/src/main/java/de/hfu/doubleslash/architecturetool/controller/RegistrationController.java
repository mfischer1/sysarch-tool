package de.hfu.doubleslash.architecturetool.controller;

import de.hfu.doubleslash.architecturetool.config.RegistrationForm;
import de.hfu.doubleslash.architecturetool.repository.UserDataRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/register")
public class RegistrationController {
    private final UserDataRepository userDataRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    public RegistrationController(UserDataRepository userDataRepository, BCryptPasswordEncoder passwordEncoder){
        this.userDataRepository=userDataRepository;
        this.passwordEncoder=passwordEncoder;
    }

    // Creates View
    @GetMapping
    public ModelAndView registerForm(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("register.html");
        return modelAndView;
    }


    // save User in Database
    @PostMapping
    public String processRegistration(RegistrationForm data){
        userDataRepository.save(data.toUser(passwordEncoder));
        return "redirect:/home";
    }
}
