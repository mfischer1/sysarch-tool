package de.hfu.doubleslash.architecturetool.domain;


import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "drag_item")
public class DragItem extends AbstractEntity {

    private String type;

    private int positionX;
    private int positionY;

    private int height;
    private int width;

    private String specificValue;

    private long objectId;

    public DragItem() {
    }

    public DragItem(long objectId, int positionX, int positionY, int height, int width, String type, String specificValue) {
        this.objectId = objectId;
        this.positionX = positionX;
        this.positionY = positionY;
        this.height = height;
        this.width = width;
        this.type = type;
        this.specificValue = specificValue;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int posX) {
        positionX = posX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int posY) {
        positionY = posY;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSpecificValue() {
        return specificValue;
    }

    public void setSpecificValue(String specificValue) {
        this.specificValue = specificValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DragItem)) {
            return false;
        }

        DragItem dragItem = (DragItem) o;

        return Objects.equals(id, dragItem.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode() * 43;
    }

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }
}
