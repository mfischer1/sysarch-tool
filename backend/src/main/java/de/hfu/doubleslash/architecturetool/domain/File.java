package de.hfu.doubleslash.architecturetool.domain;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "file")
public class File extends AbstractEntity {

    private String filePath;
    private String filename;
    private String type;

    public File() {
    }

    public File(String filePath, String filename, String type) {
        this.filePath = filePath;
        this.filename = filename;
        this.type = type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof File)) {
            return false;
        }

        File layer = (File) o;

        return Objects.equals(id, layer.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode() * 43;
    }
}
