package de.hfu.doubleslash.architecturetool.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Entity
@Table(name = "file_manager")
public class FileManager extends AbstractEntity {
    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private final List<File> files = new ArrayList<>();

    public FileManager() {
    }

    public List<File> getFiles() {
        return files;
    }

    public void addFile(File file) {
        files.add(file);
    }

    public void removeFileById(long id) {
        for (File file : files) {
            if (file.id == (id)) {
                files.remove(file);
            }
        }
    }

    public Optional<File> renameFile(long fileId, String fileTitle) {
        Optional<File> optionalFile = files.stream().filter(f -> f.id == fileId).findFirst();
        if (optionalFile.isPresent()) {
            File file = optionalFile.get();
            file.setFilename(fileTitle);
        }
        return optionalFile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FileManager)) {
            return false;
        }

        FileManager that = (FileManager) o;

        return Objects.equals(id, that.id);

    }

    @Override
    public int hashCode() {
        return files.hashCode() * 43;
    }
}
