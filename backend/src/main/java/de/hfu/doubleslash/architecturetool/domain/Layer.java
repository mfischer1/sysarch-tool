package de.hfu.doubleslash.architecturetool.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "layer")
public class Layer extends AbstractEntity {

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private final List<DragItem> dragItems = new ArrayList<>();
    private String title;
    private long parentLayerId;
    private int treeDepth;
    private int positionX;
    private int positionY;
    private int width;
    private int height;
    private int backgroundImageId;
    private long objectId;

    public Layer() {
    }

    public Layer(String title, long parentLayerId, int treeDepth, int positionX, int positionY, int width, int height, int backgroundImageId, long objectId) {
        this.title = title;
        this.parentLayerId = parentLayerId;
        this.treeDepth = treeDepth;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.backgroundImageId = backgroundImageId;
        this.objectId = objectId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addDragItem(DragItem dragItem) {
        if (dragItem != null) {
            dragItems.add(dragItem);
        }
    }

    public void deleteDragItem(DragItem dragItem) {
        if (dragItem != null) {
            dragItems.remove(dragItem);
        }
    }

    public long getParentLayerId() {
        return parentLayerId;
    }

    public void setParentLayerId(long parentLayerId) {
        this.parentLayerId = parentLayerId;
    }

    public int getTreeDepth() {
        return treeDepth;
    }

    public void setTreeDepth(int treeDepth) {
        this.treeDepth = treeDepth;
    }

    public int getPositionX() {
        return positionX;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBackgroundImageId() {
        return backgroundImageId;
    }

    public void setBackgroundImageId(int backgroundImageId) {
        this.backgroundImageId = backgroundImageId;
    }

    public List<DragItem> getDragItems() {
        return dragItems;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Layer)) {
            return false;
        }

        Layer layer = (Layer) o;

        return Objects.equals(id, layer.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode() * 43;
    }

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }
}
