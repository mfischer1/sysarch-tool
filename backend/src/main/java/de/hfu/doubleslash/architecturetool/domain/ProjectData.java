package de.hfu.doubleslash.architecturetool.domain;

import de.hfu.doubleslash.architecturetool.util.Utils;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "project_data")
public class ProjectData extends AbstractEntity {


    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    private List<Layer> layers = new ArrayList<>();
    private String projectName;
    private String projectDescription;
    @OneToOne
    private UserData userData;
    private int workbenchWidth;
    private int workbenchHeight;
    private String creationDate;
    private String lastModified;
    @OneToOne(fetch = FetchType.EAGER)
    private FileManager fileManager;

    public ProjectData() {
    }

    public ProjectData(String title, UserData userData, String projectDescription, int workbenchWidth, int workbenchHeight) {
        projectName = title;
        this.projectDescription = projectDescription;
        this.userData = userData;
        creationDate = Utils.getActualDateAndTime();
        lastModified = creationDate;
        fileManager = new FileManager();
        this.workbenchWidth = workbenchWidth;
        this.workbenchHeight = workbenchHeight;

    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public UserData getUser() {
        return userData;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public void addLayer(Layer layer) {
        if (layer != null) {
            layers.add(layer);
        }
    }

    public List<Layer> getLayers() {
        return layers;
    }

    public void deleteLayer(Layer layer) {
        if (layer != null) {
            System.out.println("delete " + layer.id + " " + layer.getObjectId());
            layers.remove(layer);
        }
    }

    public List<File> getFiles() {
        return fileManager.getFiles();
    }

    public FileManager getFileManager() {
        return fileManager;
    }

    public void setFileManager(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }

    public UserData getUserData() {
        return userData;
    }

    public void setUserData(UserData userData) {
        this.userData = userData;
    }

    public int getWorkbenchWidth() {
        return workbenchWidth;
    }

    public void setWorkbenchWidth(int workbenchWidth) {
        this.workbenchWidth = workbenchWidth;
    }

    public int getWorkbenchHeight() {
        return workbenchHeight;
    }

    public void setWorkbenchHeight(int workbenchHeight) {
        this.workbenchHeight = workbenchHeight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProjectData)) {
            return false;
        }
        ProjectData projectData = (ProjectData) o;
        return id.equals(projectData.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode() * 43;
    }

    public void addFile(File file) {
        fileManager.addFile(file);
    }

    public Long getId() {
        return this.id;
    }
}
