package de.hfu.doubleslash.architecturetool.domain;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "user_data")
public class UserData extends AbstractEntity {

    @Column(nullable = false, unique = true)
    private String name;

    private String password;

    public UserData() {
    }

    public UserData(String name) {
        this.name = name;
    }

    public UserData(String name, String password){
        this.name=name;
        this.password=password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserData)) return false;
        UserData userData = (UserData) o;
        return Objects.equals(id, userData.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode() * 43;
    }


//    public UserData toUser(BCryptPasswordEncoder passwordEncoder){
//        return new UserData(name, passwordEncoder.encode(password));
//    }
}
