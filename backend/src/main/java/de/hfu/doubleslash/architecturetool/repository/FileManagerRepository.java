package de.hfu.doubleslash.architecturetool.repository;

import de.hfu.doubleslash.architecturetool.domain.FileManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileManagerRepository extends JpaRepository<FileManager, Integer> {
    FileManager findByid(int id);
}
