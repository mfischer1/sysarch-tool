package de.hfu.doubleslash.architecturetool.repository;


import de.hfu.doubleslash.architecturetool.domain.ProjectData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectDataRepository extends JpaRepository<ProjectData, Long> {
    ProjectData findByProjectName(String title);
}
