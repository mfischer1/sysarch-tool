package de.hfu.doubleslash.architecturetool.repository;

import de.hfu.doubleslash.architecturetool.domain.UserData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDataRepository extends JpaRepository<UserData, Long> {
    UserData findByName(String name);
}
