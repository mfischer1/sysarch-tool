package de.hfu.doubleslash.architecturetool.service;

import de.hfu.doubleslash.architecturetool.domain.UserData;
import de.hfu.doubleslash.architecturetool.domain.UserDataPrincipal;
import de.hfu.doubleslash.architecturetool.repository.UserDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDataDetailsService implements UserDetailsService {

    @Autowired
    private UserDataRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        UserData userData = userRepository.findByName(username);
        if (userData == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserDataPrincipal(userData);
    }

    @Bean
    public BCryptPasswordEncoder encoder(){
        return new BCryptPasswordEncoder(10);
    }
}
