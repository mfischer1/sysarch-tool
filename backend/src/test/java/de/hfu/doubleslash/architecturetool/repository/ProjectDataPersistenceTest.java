package de.hfu.doubleslash.architecturetool.repository;


import de.hfu.doubleslash.architecturetool.domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class ProjectDataPersistenceTest {
    private static final String PROJECTNAME = "REST API";

    @Autowired
    private ProjectDataRepository projectDataRepository;
    @Autowired
    UserDataRepository userDataRepository;
    @Autowired
    FileManagerRepository fileManagerRepository;

    UserData expectedUserData;
    ProjectData expectedProjectData;
    Layer expectedLayer;
    DragItem expectedDragItem;
    File expectedFile;

    @Test
    @Transactional
    public void testFetchData() {
        //create a user and persist
        expectedUserData = new UserData("Enes Oezel");
        userDataRepository.save(expectedUserData);

        //create a project
        expectedProjectData = new ProjectData(PROJECTNAME, expectedUserData, "A rest API", 0, 0);

        //create a layer and add to project
        expectedLayer = new Layer("First", 0, 0, 0, 0, 0, 0, 0, 0);
        expectedProjectData.addLayer(expectedLayer);

        //create dragItem and add to layer
        expectedDragItem = new DragItem(0, 0, 0, 0, 0, "", "");
        expectedLayer.addDragItem(expectedDragItem);

        //create and add file to project
        expectedFile = new File();
        expectedProjectData.addFile(expectedFile);
        fileManagerRepository.save(expectedProjectData.getFileManager());

        //persist the project
        projectDataRepository.save(expectedProjectData);

        //Extract data from database

        ProjectData actualProjectData = projectDataRepository.findByProjectName(PROJECTNAME);
        assertEquals(actualProjectData, expectedProjectData);

        UserData actualUserData = actualProjectData.getUser();
        assertEquals(actualUserData, expectedUserData);

        Layer actualLayer = actualProjectData.getLayers().get(0);
        assertEquals(actualLayer, expectedLayer);

        FileManager actualFileManager = actualProjectData.getFileManager();
        assertEquals(actualFileManager, expectedProjectData.getFileManager());

        DragItem actualDragItem = actualLayer.getDragItems().get(0);
        assertEquals(actualDragItem, expectedDragItem);

    }
}
