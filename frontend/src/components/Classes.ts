import {Configuration} from "@/components/Configuration";
import axios from 'axios';
import $ from 'jquery';

export class UserData {
    public id: number;
    public username: string;
    public projects: any[] = [];

    public constructor(id: number, username: string) {
        this.id = id;
        this.username = username;
    }

    public addProjectEntry(id: number, title: string): void {
        // @ts-ignore
        this.projects.push({'id': id, 'title': title});
    }

    public removeProjectEntryById(id: number): void {
        let newProjects: any[] = [];

        for (let projectEntry of this.projects as any[]) {
            if (projectEntry.id != id) {
                newProjects.push(projectEntry);
            }
        }

        this.projects = newProjects;
    }

    public refreshProjectEntryTitle(id: number, title: string): void {
        for (let projectEntry of this.projects as any[]) {
            if (projectEntry.id == id) {
                projectEntry.title = title;
                break;
            }
        }
    }
}

export class ProjectData {
    public projectId: number = 0;
    public workbenchWidth: number = Configuration.DEFAULT_WORKBENCH_WIDTH;
    public workbenchHeight: number = Configuration.DEFAULT_WORKBENCH_HEIGHT;
    public projectName: string = '';
    public projectDescription: string = '';
    public files: FileData[] = [];
}

export class FileData {
    public static ID_NO_IMAGE: number = -100;
    public static ID_INHERITED_IMAGE: number = -200;

    public static TYPE_IMAGE: string = 'image';
    public static TYPE_DOCUMENT: string = 'document';

    public id: number;
    public type: string;
    public filename: string;
    public fileAddress: string;
    public title: string;

    public constructor(id: number, type: string, filename: string, fileAddress: string, title: string) {
        this.id = id;
        this.type = type;
        this.filename = filename;
        this.fileAddress = fileAddress;
        this.title = title;
    }
}

export class Layer {
    public readonly id: number;
    public readonly objectId: number;
    public title: string;
    public parentLayerId: number;
    public treeDepth: number;
    public positionX: number;
    public positionY: number;
    public width: number;
    public height: number;
    public backgroundImageId: number;
    public dragItemsMap: { [key: number]: DragItem } = {};
    public dragItems: DragItem[] = [];

    public constructor(id: number, title: string, parentLayerId: number = -1, treeDepth: number = 0, positionX: number = 0, positionY: number = 0, width: number = -1, height: number = -1, backgroundImageId: number = FileData.ID_NO_IMAGE) {
        this.id = id;
        this.objectId = id;
        this.title = title;
        this.parentLayerId = parentLayerId;
        this.treeDepth = treeDepth;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.backgroundImageId = backgroundImageId;
    }

    public setDragItem(dragItem: DragItem): void {
        this.dragItemsMap[dragItem.getId()] = dragItem;
    }

    public normalize(): void {
        this.positionX = Utils.formatInt(this.positionX);
        this.positionY = Utils.formatInt(this.positionY);
        this.width = Utils.formatInt(this.width);
        this.height = Utils.formatInt(this.height);

        for (let dragItem of this.dragItems as DragItem[]) {
            dragItem.normalize();
        }
    }
}

export class DragItem {
    public static TYPE_IMAGE: string = 'image';
    public static TYPE_LABEL: string = 'label';
    public static TYPE_TRAFFIC_LIGHT: string = 'traffic-light';
    public static TYPE_DOCUMENT: string = 'document';
    public static TYPE_RISK: string = 'risk';
    public static TYPE_NOTICE: string = 'notice';
    public static TYPE_MAGNIFIER: string = 'magnifier';
    public readonly objectId: number;
    public positionX: number | null = null;
    public positionY: number | null = null;
    public specificValue: any;
    private readonly id: number;
    private readonly type: string;
    private readonly filepath: string = '';
    private readonly height: number = 0;
    private readonly width: number = 0;

    public constructor(id: number, type: string) {
        this.id = id;
        this.objectId = id;
        this.type = type;
    }

    public getId(): number {
        return this.id;
    }

    public getType(): string {
        return this.type;
    }

    public normalize(): void {
        if (this.positionX != null) {
            this.positionX = Utils.formatInt(this.positionX);
        }

        if (this.positionY != null) {
            this.positionY = Utils.formatInt(this.positionY);
        }
    }
}

export class ModalData {
    public heading: string;
    public content: string;
    public acceptButtonText: string;
    public cancelButtonText: string;
    public acceptCallback: any;
    public cancelCallback: any;
    public firstActionCallback: any;
    public firstActionText: string;
    public secondActionCallback: any;
    public secondActionText: string;
    public callbackData: any;

    public constructor(heading: string, content: string, acceptButtonText: string, cancelButtonText: string, acceptCallback: any, cancelCallback: any, firstActionCallback: any, firstActionText: string, secondActionCallback: any, secondActionText: string, callbackData: any) {
        this.heading = heading;
        this.content = content;
        this.acceptButtonText = acceptButtonText;
        this.cancelButtonText = cancelButtonText;
        this.acceptCallback = acceptCallback;
        this.cancelCallback = cancelCallback;
        this.firstActionCallback = firstActionCallback;
        this.firstActionText = firstActionText;
        this.secondActionCallback = secondActionCallback;
        this.secondActionText = secondActionText;
        this.callbackData = callbackData;
    }
}

export class State {
    private static instance: State | null = null;

    public data: any = {};
    public layers: { [key: number]: Layer } = {};

    constructor() {
        this.data.currentProjectData = new ProjectData();
        this.data.currentLayerId = null;
    }

    public static get(): State {
        if (this.instance == null) {
            this.instance = new State();
        }

        return this.instance;
    }

    public static reset(): void {
        this.instance = null;
    }

    public resetLayers(): void {
        this.layers = {};
    }

    public openLayer(id: number): boolean {
        if (this.layers[id] != null) {
            this.data.currentLayerId = id;

            return true;
        }

        return false;
    }

    public getCurrentLayerId(): number {
        return this.data.currentLayerId;
    }

    public getCurrentLayer(): Layer {
        return this.layers[this.getCurrentLayerId()];
    }

    public getLayer(id: number): Layer | null {
        return this.layers[id];
    }

    public setLayer(layer: Layer): void {
        this.layers[layer.id] = layer;
    }

    public prepareDefaultLayer(): void {
        if (State.get().getLayer(0) == null) {
            State.get().setLayer(new Layer(0, 'Main layer'));
        }

        State.get().data.currentLayerId = 0;
    }

    public getLayersAsList(): Layer[] {
        let layers: Layer[] = [];

        for (const key of Object.keys(this.layers)) {
            let layer = $.extend(true, {}, this.layers[parseInt(key)]);

            layer.dragItems = [];

            for (const dragItemKey of Object.keys(layer.dragItemsMap)) {
                layer.dragItems.push(layer.dragItemsMap[parseInt(dragItemKey)]);
            }

            delete layer.dragItemsMap;

            layer.normalize();

            layers.push(layer);
        }

        return layers;
    }

    public removeLayer(layerId: number): void {
        delete this.layers[layerId];
    }

    public removeLayerWithSubLayers(layerId: number): void {
        let newLayers: { [key: number]: Layer } = {};

        let children: number[] = this.findChildLayerIds(layerId);

        for (const key of Object.keys(this.layers)) {
            let currentLayerId: number = parseInt(key);

            if (currentLayerId != layerId) {
                if (!children.includes(currentLayerId)) {
                    newLayers[currentLayerId] = this.layers[currentLayerId];
                }
            }
        }

        this.layers = newLayers;
    }

    public getDragItem(id: number): DragItem | null {
        return this.layers[this.data.currentLayerId].dragItemsMap[id];
    }

    public setDragItem(dragItem: DragItem): void {
        this.layers[this.data.currentLayerId].setDragItem(dragItem);
    }

    public removeDragItem(dragItemId: number): void {
        delete this.layers[this.data.currentLayerId].dragItemsMap[dragItemId];
    }

    public setUser(id: number, username: string): void {
        this.data.userData = new UserData(id, username);
    }

    public getUser(): UserData | null {
        return this.data.userData;
    }

    public getFileData(id: number): FileData | null {
        for (let fileData of this.data.currentProjectData.files as FileData[]) {
            if (fileData.id == id) {
                return fileData;
            }
        }

        return null;
    }

    private findChildLayerIds(parentId: number): number[] {
        let children: number[] = [];

        for (const key of Object.keys(this.layers)) {
            let layer: Layer = this.layers[parseInt(key)];

            if (layer != null) {
                if (layer.parentLayerId == parentId) {
                    children = children.concat(this.findChildLayerIds(layer.id));
                }
            }
        }

        return children;
    }
}

export class Utils {
    public static generateId(): number {
        //return seconds since 1 January 1970 as ID
        return (Date.now() * 1000);
    }

    public static cropImage(imgObj: any, newWidth: number, newHeight: number, startX: number, startY: number, ratio: number): any {
        if (imgObj == null) {
            return '';
        }

        let tnCanvas: any = document.createElement('canvas');

        let tnCanvasContext: any = tnCanvas.getContext('2d');

        if (tnCanvasContext == null) {
            return null;
        }

        tnCanvas.width = newWidth;
        tnCanvas.height = newHeight;

        let bufferCanvas: any = document.createElement('canvas');
        let bufferContext: any = bufferCanvas.getContext('2d');

        bufferCanvas.width = imgObj.width;
        bufferCanvas.height = imgObj.height;

        bufferContext.drawImage(imgObj, 0, 0);

        tnCanvasContext.drawImage(bufferCanvas, startX, startY, (newWidth * ratio), (newHeight * ratio), 0, 0, newWidth, newHeight);

        return tnCanvas.toDataURL();
    }

    public static performGetRequest(endpoint: string, onSuccess: any, onError: any): void {
        axios.get(Configuration.BACKEND_API_ADDRESS + endpoint)
            .then(function (this: any, response: any) {
                if ((response.hasOwnProperty('status')) && (response.status == 200)) {
                    onSuccess(response);
                } else {
                    onError(response);
                }
            })
            .catch(function (error: any) {
                onError(error);
            });
    }

    public static formatForDatabase(content: string): string {
        return content = content.replace(/\n/g, '<br>');
    }

    public static formatFromDatabase(content: string): string {
        return content = content.replace(/<br>/g, '\n');
    }

    public static base64Encode(str: any): string {
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
            (match, p1) => {
                return String.fromCharCode(("0x" + p1) as any);
            }));
    }

    public static base64Decode(str: any): string {
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    public static clog(content: any): void {
        console.log(content);
    }

    public static debug(content: any): void {
        if (Configuration.SHOW_DEBUG_MESSAGES) {
            this.clog(content);
        }
    }

    public static userMessage(content: any): void {
        this.clog(content);
        alert(content);
    }

    public static formatInt(value: number): number {
        return Math.round(value);
    }
}

export class TestOnStartup {
    public static performTests(): void {
    }
}

export class JSONObject {
    private data: any;

    public static fromJsonString(json: string): JSONObject | null {
        try {
            let jsonObject: JSONObject = new JSONObject();
            jsonObject.data = JSON.parse(json);
            return jsonObject;
        } catch (e) {
            return null;
        }
    }

    public static fromObject(object: any): JSONObject {
        let jsonObject: JSONObject = new JSONObject();
        jsonObject.data = object;
        return jsonObject;
    }

    public get(key: string): any {
        if (this.data.hasOwnProperty(key)) {
            return this.data[key];
        }

        return null;
    }

    public getPath(keys: string[]): any {
        let currentData: any = this.data;

        for (let currentKey of keys) {
            if (currentData.hasOwnProperty(currentKey)) {
                currentData = currentData[currentKey];
            } else {
                return null;
            }
        }

        return currentData;
    }

    public set(key: string, value: any): void {
        this.data[key] = value;
    }
}
