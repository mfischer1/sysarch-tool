export class Configuration {
    public static BACKEND_FILE_ADDRESS: string = 'https://sysarch-tool.herokuapp.com/file/get/';
    public static BACKEND_API_ADDRESS: string = 'https://sysarch-tool.herokuapp.com/api/'; //'http://localhost:8080/api/';

    public static DEFAULT_WORKBENCH_WIDTH: number = 1600;
    public static DEFAULT_WORKBENCH_HEIGHT: number = 700;

    public static MOCK: boolean = false;
    public static SHOW_DEBUG_MESSAGES: boolean = false;
}
