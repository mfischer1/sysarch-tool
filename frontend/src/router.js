import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name : "Home",
        component: () =>
            import("./components/Home")
    },
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: "/info",
        name: "Info",
        component: () =>
            import("./components/Info")
    },
    {
        path: "/login",
        name: "Login",

        component: () =>
            import("./components/Login")
    },
    {
        path: "/modal",
        name: "Modal",
        component: () =>
            import("./components/Modal")
    },
    {
        path: "/nothingToShow",
        name: "NothingToShow",
        component: () =>
            import("./components/NothingToShow")
    },
    {
        path: "/notLoggedIn",
        name: "NotLoggedIn",
        component: () =>
            import("./components/NotLoggedIn")

    },
    {
        path: "/projectFiles",
        name: "ProjectFiles",
        component: () =>
            import("./components/ProjectFiles"),
        meta: {
            requireAuth: true
        }
    },
    {
        path: "/projectOverview",
        name: "ProjectOverview",
        component: () =>
            import("./components/ProjectOverview"),
        meta: {
            requireAuth: true
        }
    },
    {
        path: "/projectSettings",
        name: "ProjectSettings",
        component: () =>
            import("./components/ProjectSettings"),
        meta: {
            requireAuth: true
        }
    },
];

const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requireAuth)) {
        if (store.getters.isLoggedIn) {
            next();
        } else {
            next({
                path: "/login",
                params: { nextUrl: to.fullPath }
            });
        }
    } else {
        next();
    }
});

export default router;