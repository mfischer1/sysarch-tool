module.exports = {
        devServer: {
            proxy: {
                '/api': {
                    target: 'https://sysarch-tool.herokuapp.com',
                    ws: true,
                    changeOrigin: true
                },
                "/register": {
                    target: "http://localhost:8080",
                    secure: false
                },
                "/": {
                    target: "http://localhost:8080",
                    secure: false
                }
}
    },
  /*outputDir: '../backend/src/main/resources/public',*/
  outputDir: 'target/dist',
  assetsDir: 'assets'
};
